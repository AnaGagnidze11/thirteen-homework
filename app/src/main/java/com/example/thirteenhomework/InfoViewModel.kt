package com.example.thirteenhomework


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class InfoViewModel : ViewModel() {
    private val infoLiveData = MutableLiveData<Result<List<ItemModel>>>().apply {
        mutableListOf<ItemModel>()
    }
    val _infoLiveData: LiveData<Result<List<ItemModel>>> = infoLiveData


    fun initInfo() {
        CoroutineScope(Dispatchers.IO).launch {
            getInfo()
        }
    }


    private suspend fun getInfo() {
        infoLiveData.postValue(Result.loading(true))
        val result = RetrofitService.retrofitService().getInfo()
        if (result.isSuccessful) {
            val items = result.body()
            infoLiveData.postValue(Result.success(items!!))
        } else {
            infoLiveData.postValue(Result.error(result.message()))
        }

    }
}