package com.example.thirteenhomework

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thirteenhomework.databinding.InfoFragmentBinding
import com.google.gson.Gson

class InfoFragment : Fragment() {

    private lateinit var binding: InfoFragmentBinding

    private val viewModel: InfoViewModel by viewModels()

    private lateinit var adapter: RecyclerViewAdapter

    private val news = mutableListOf<ItemModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = InfoFragmentBinding.inflate(inflater, container, false)
        initEverything()
        return binding.root
    }


    private fun initEverything() {
        viewModel.initInfo()
        initRecyclerView()
        observing()
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        adapter = RecyclerViewAdapter(binding.recyclerView)
        binding.recyclerView.adapter = adapter
        binding.refresh.setOnRefreshListener {
            adapter.clearItems()
            viewModel.initInfo()
        }
        adapter.setOnLoadMoreListener(loadMoreListener)
    }


    private val loadMoreListener = object :
        OnLoadMoreListener {
        override fun onLoadMore() {
            if (news.size != 0) {
                if (!news[news.size - 1].isLast) {
                    binding.recyclerView.post {
                        val itemModel = ItemModel()
                        itemModel.isLast = true
                        news.add(itemModel)
                        adapter.addItems(itemModel)
                        viewModel.initInfo()
                    }
                }
            }
        }
    }




    private fun observing() {

        viewModel._infoLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                Result.Status.SUCCESS -> {

                    val lastPosition = news.size
                    if (lastPosition > 0) {
                        news.removeAt(news.size - 1)
                        adapter.notifyItemRemoved(news.size)
                    }
                    adapter.setLoaded()
                    news.addAll(it.data!!.toMutableList())

                    if (news.isNotEmpty() && news.size != 1) adapter.notifyItemMoved(
                        lastPosition,
                        news.size - 1
                    )
                    else adapter.notifyDataSetChanged()
                    adapter.setItems(news)

                    binding.refresh.isRefreshing = it.refreshing
                }
                Result.Status.ERROR -> {
                    Toast.makeText(requireActivity(), "${it.message}", Toast.LENGTH_SHORT).show()
                    binding.refresh.isRefreshing = it.refreshing
                }

                Result.Status.LOADING -> binding.refresh.isRefreshing = it.refreshing

            }

        })
    }


}