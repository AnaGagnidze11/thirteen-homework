package com.example.thirteenhomework

import androidx.paging.PagedList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRequests {
    @GET("news")
    suspend fun getInfo(): Response<List<ItemModel>>
}