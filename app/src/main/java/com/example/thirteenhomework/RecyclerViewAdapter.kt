package com.example.thirteenhomework

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.thirteenhomework.databinding.ItemModelLayoutBinding
import com.example.thirteenhomework.databinding.LoadingLayoutBinding


class RecyclerViewAdapter( private val recyclerView: RecyclerView) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_LOADING = 1
    }

    private val items = mutableListOf<ItemModel>()

    private var onLoadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private val visibleThreshold = 5

    init {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    if (onLoadMoreListener != null)
                        onLoadMoreListener!!.onLoadMore()
                    isLoading = true
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            VIEW_TYPE_ITEM -> ItemViewHolder(
                ItemModelLayoutBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
            else -> LoadMorePostsViewHolder(
                LoadingLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemViewHolder -> holder.bind()
        }
    }

    override fun getItemCount() = items.size


    inner class ItemViewHolder(private val binding: ItemModelLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var item: ItemModel
        fun bind() {
            item = items[adapterPosition]
            binding.titleTextView.text = item.titleKA
            Glide.with(binding.root.context).load(item.cover).into(binding.newsCoverImageView)
        }

    }

    inner class LoadMorePostsViewHolder(private val binding: LoadingLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setItems(items: MutableList<ItemModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener
    }

    fun setLoaded() {
        isLoading = false
    }

    fun addItems(item: ItemModel){
        items.add(item)
        notifyItemInserted(items.size)
    }

    fun clearItems(){
        this.items.clear()
        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        return when {
            items[position].isLast -> VIEW_TYPE_LOADING
            else -> VIEW_TYPE_ITEM
        }
    }
}