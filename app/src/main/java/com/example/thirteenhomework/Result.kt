package com.example.thirteenhomework

data class Result<T>(val status: Status, val data: T? = null, val message: String? = null, val refreshing: Boolean = false) {

    enum class Status{
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object{
        fun <T> success(data: T): Result<T> {
            return Result(Status.SUCCESS, data)
        }

        fun <T> error(message: String): Result<T> {
            return Result(Status.ERROR, null, message)
        }

        fun <T> loading(refreshing: Boolean): Result<T> {
            return Result(Status.LOADING, null, null, refreshing)
        }
    }
}