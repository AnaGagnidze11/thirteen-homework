package com.example.thirteenhomework

data class ItemModel(val titleKA: String = "", val cover: String = "", var isLast: Boolean = false)
