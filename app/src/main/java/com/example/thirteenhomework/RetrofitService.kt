package com.example.thirteenhomework

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val BASE_URL = "http://139.162.207.17/api/m/v2/"

    fun retrofitService(): ApiRequests {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(ApiRequests::class.java)
    }
}