package com.example.thirteenhomework

interface OnLoadMoreListener {
    fun onLoadMore()
}